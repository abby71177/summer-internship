
#include<iostream>
#include<vector>
using namespace std;
class Student {
	string name;
	string id;
public:
	Student():name("Unknown"),id("00"){}
	explicit Student(string nameS,string Id):name(nameS),id(Id){}
	void changeInfoStudent(string stud, string Id) {
		id = Id;
		name = stud;
	}
	   void returnDetails2()  {
		cout <<"Student Details"<< this->name << endl << this->id;
	}
	  ~Student() { cout << "student destructor"<<this->name<<endl; }


};
class Teacher :public Student {
	string teacherName;
	string id;
	shared_ptr <Teacher> teacher;
public:
	Teacher():teacherName("Unknown"),id("OO"){}
	explicit Teacher(string nameT, string Id) :teacherName(nameT), id(Id) {}

	void changeInfo(string nameT, string Id,string stud,string Id2) {
		changeInfoStudent(stud, Id2);
		teacherName = nameT;
		Teacher::id = Id;
	}
	void returnPointer(shared_ptr<Teacher> ptr) {
		teacher = ptr;
		teacher->returnDetails2();

		teacher->returnDetails();
	
	}
	 void returnDetails()  {
		 //teacher->returnDetails();
		cout << "\nTeacher Details"<<endl<<teacherName << endl << id<<endl;

	}
	 ~Teacher() { cout << "teacher destructor" << this->teacherName << endl; }
};
void display(Teacher& obj) {
	obj.returnDetails();
}
int main() {
	unique_ptr<Teacher>* ptr(Teacher()); //when goes out of scope will delete heap mem
	                                     //cant copy a unique pointer
	unique_ptr<Teacher> ptr2 = make_unique<Teacher>();
	//ptr2->returnDetails();
	shared_ptr<Teacher> ptr3=make_shared<Teacher>();
	ptr3->changeInfo("Teacher A", "A001", "Student A", "SA001");
	ptr3->returnPointer(ptr3);
	weak_ptr<Student> weakPtr = ptr3;
	vector<weak_ptr <Student>> students;
	students.push_back(make_shared<Teacher>("Abirami", "007"));
	students.push_back(make_shared<Teacher>("Benjamin", "0037"));
	students.push_back(make_shared<Teacher>("Chirs", "0207"));
	cout << students[0].use_count();
	
	











	



	return 0;

}
