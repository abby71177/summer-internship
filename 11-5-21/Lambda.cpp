#include<iostream>

int globalNumber = 100;
auto G_Lambda = [](int a, int b) {
	return ((a > b) ? true : false);
};


int main() {
	int number = 10;

	auto Lambda = [=]() mutable  { //bydefault is pass by reference
		//globalNumber++;
		number++;           //can only be done if mutable and either = or & must be mentioned
		std::cout << number;

	};
	Lambda();
	std::cout << number << std::endl; //11 when passed by & and 10 when passed by =
	std::cout << G_Lambda(10, 20);
}
