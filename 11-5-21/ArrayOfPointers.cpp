#include<iostream>
#define MAX_SIZE 10
void printBoundary(int (*ptr)[MAX_SIZE], int rows, int cols) {
	int index1, index2;
	for ( index1 = 0;index1 < rows;index1++) {
		std::cout << std::endl;
		for ( index2 = 0;index2 < cols;index2++) {
			if (index1 == 0 || index1 == rows - 1 || index2 == 0 || index2 == cols - 1)
				std::cout << *(*(ptr + index1) + index2) << " ";
			else
				std::cout << "  ";

		}
	}
}

void changeElements(int* ptr) {
	for (int index = 0;index < MAX_SIZE;index++)
		*(ptr + index) = index + 10000;
}

int main() {
	int arr[MAX_SIZE],*ptr,index,rows,cols;
	ptr = arr;
	for ( index = 0;index < MAX_SIZE;index++)
		*(ptr + index) = index + 100;
	std::cout << "Size of Array: " << sizeof(arr) / sizeof(arr[0]) << std::endl;
	std::cout << "contents of array before changing" << std::endl;
	for (index = 0;index < MAX_SIZE;index++)
		std :: cout << arr[index] << " ";

	changeElements(arr);                          //arrays by default are pass by refernce
	std::cout << "contents of array after changing" << std::endl;
	for (index = 0;index < MAX_SIZE;index++)
		std::cout << arr[index] << " ";

	int *arr1 = new int[MAX_SIZE];
	for (index = 0;index < MAX_SIZE;index++)
		*(arr1 + index) = index + 1000;
	std::cout << std::endl;
	for (index = 0;index < MAX_SIZE;index++)
		std::cout << arr1[index] << " ";
	delete[]arr1;

	std::cout << "enter rows and columns of matrix: " << std::endl;
	std::cin >> rows >> cols;
	int array2D [MAX_SIZE][MAX_SIZE];
	for (index = 0;index < rows;index++)
		for (int indexj = 0;indexj < cols;indexj++)
			std::cin >> array2D[index][indexj];
	printBoundary(array2D, rows, cols);

	char arrayOfNames[MAX_SIZE][MAX_SIZE];
	char (*charPtr)[MAX_SIZE];
	charPtr = arrayOfNames;
	for (index = 0;index < MAX_SIZE;index++)
	{
		std::cout << "\nenter name:";
		std::cin >> *(charPtr+index);
	}
	for (index = 0;index < MAX_SIZE;index++)
		std::cout << *(charPtr + index)<<std::endl;


	return 0;
	
}
