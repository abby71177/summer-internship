#include<iostream>

class Book {
	const char* bookName;
	int bookId;
public:
	Book()
		:bookName("Sumita Arora"), bookId(100) {}
	Book(const char *name, int id)
		:bookName(name), bookId(id) {}
	void display() {
		std::cout << bookName << " " << bookId;
	}
};
int main() {
	Book object;
	Book object1("CS", 99);
	object.display();
	object1.display();
}
