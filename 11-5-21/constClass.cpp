#include<iostream>

 class Demo {
	//const int number;
	mutable int number;
public:
	Demo() {
		number = 100;  //cannot be done if const
	}
	void displayNumber() const {
		number++;                  //const method can acces a non const variable if it is entitled as mutable
		std::cout << "Const function "<<number; 
	}
	void displayNumber()  {
		std::cout <<"Non const function"<< number;  //doesnot display 101 
	}
};
int main() {
	const Demo obj;
	obj.displayNumber();  //const object can only call a const method
	Demo obj1;
	obj1.displayNumber();
}
