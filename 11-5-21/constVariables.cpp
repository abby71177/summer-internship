#include<iostream>
const float pi = 3.14;
#define MAX(a,b) ((a>b)?a:b) //macros
#define MAX3(a,b,c) ((a>b && a>c)?a:((b>c)?b:c));

int main() {
	const char* word = "Hell\0o";
	std::cout << word;
	int number = 99;
	const int* ptr;
	ptr = &number;
	std::cout << std::endl<<*ptr;
	//*ptr = 100; //unmutable
	number = 100;
	std::cout << std::endl<<*ptr;

	const int*  const PTR = new int;
	//PTR = &number; //immutable too
	//*PTR = 2; //imutable meaning one cant modigy the value itself stored by the pointer
	std::cout << *PTR; //displays default value




	//word[0] = 'A';//cannot be done
	float pi = 3.141;
	std::cout << ::pi;  //scope resolution operator
	std::cout << std::endl;
	std::cout<<"Maximim of 5 and 10 is "<<MAX(5, 10);
	std::cout << "Maximim of 3 numbers using Ternary operator is" << MAX3(15, 108, 5);
	return 0;

}
