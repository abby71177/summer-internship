/*
Secret Agent Password
Mubashir is going on a secret mission. He needs your help but you have to learn how to encode a secret password to communicate safely with other agents. Create a function that takes an argument message and returns the encoded password.

There are some variations on the rules of encipherment. One version of the cipher rules are outlined below:

secretPassword("mubashirh") ➞ "hsajsi13u2"
Step 1: Message length should be of nine characters containing only lowercase letters from 'a' to 'z'. If the argument doesn't meet this requirement you must return "BANG! BANG! BANG!" (Remember, there are no second chances in the spy world!)

Step 2: Divide the string into three equal parts of three characters each:

1 - mub
2 - ash
3 - irh
Step 3: Convert the first and third letter to the corresponding number, according to the English alphabets (ex. a = 1, b = 2, c = 3 ... z = 26, etc).

mub = 13u2
Step 4: Reverse the fourth, fifth, and sixth letters:

ash = hsa
Step 5: Replace seventh, eighth, and ninth letter with next letter (z will be substituted with a).

irh = jsi
Step 6: Return the string in the following order: "Part_2+Part_3+Part_1"

"hsajsi13u2"
See the below examples for a better understanding:

Examples
secretPassword("mubashirh") ➞ "hsajsi13u2"

secretPassword("mattedabi") ➞ "detbcj13a20"

secretPassword("HeLen-eda") ➞ "BANG! BANG! BANG!"
// Length is not equal to 9
// Contains characters other than lower alphabets*/



using std::to_string;

#include<cctype>
std::string secretPassword(std::string message) {
	int index,current,lowerCase=0;
	std::string part1("");
	std::string part2("");
	std::string part3("");
	for(index=0;index<message.length();index++){
		if(isalpha(message[index]))
			 if(islower(message[index]))
				 lowerCase++;
	}
	if(lowerCase==9){
		for(index=0;index<3;index++){
			if(index==1) part1+=message[index];
			else{
			current=message[index]-'a';
			current++;
			part1+=to_string(current);
		}
		}
		for(index=5;index>=3;index--)
			part2+=message[index];
		
		for(index=6;index<=8;index++){
			char alphabet=message[index];
			if(alphabet=='z') alphabet='a';
			else alphabet++;
			part3+=alphabet;
		}
		std::string result("");
		result+=part2;
		result+=part3;
		result+=part1;
		return result;
	}
	else
		return "BANG! BANG! BANG!";
			
	
}
