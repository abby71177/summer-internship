/*
I Put My Prime Down, Flip It and Reverse It
A prime number is a number whose only proper (non-self) divisor is 1 (example 13).

An emirp (prime spelled backwards) is a non-palindromic prime which, when its digits are reversed, makes another prime. E.g. 13 is a prime, and so is 31. Both are therefore emirps.

A bemirp is a prime which is an emirp (makes another prime with its digits reversed), but additionally, makes another prime when flipped upside down (see note). The upside-down version is also an emirp, which makes a group of 4 primes. Bemirps consist only of digits 0, 1, 6, 8, and 9.

To illustrate: 11061811, reversed = 11816011, upside-down = 11091811, upside-down reversed = 11819011. All four are primes.

Create a function that takes a number and returns "B" if it’s a bemirp, "E" if it's a (non-bemirp) emirp, "P" if it's a (non-emirp) prime, or "C" (composite / non-prime).

Examples
bemirp(101) ➞ "P"

bemirp(1011) ➞ "C"

bemirp(1069) ➞ "E"

bemirp(1061) ➞ "B"
Notes
6 upside-down is 9 and vice-versa. 0, 1, and 8 are unchanged when flipped. The remaining five digits are unflippable. */
bool prime(int num){
	if(num==2) return true;
	int flag=1;
	for(int i=2;i<num;i++)
		if(num%i==0) flag++;
  if(flag==1) return true;
	else return false;
		}

int reverseNum(int num){
	int sum1=0,rem;
	while(num!=0){
		rem=num%10;
		sum1=sum1*10+rem;
		num/=10;
	}
	return sum1;
}
char bemirp(int num){
	std::vector<int> n;
	int r,i;
	int normal=num;
	int reversed=reverseNum(num);
	while(num!=0){
       int r=num%10;
       if(r==6) r=9;
       else if(r==9) r=6;
       n.push_back(r);
       num/=10;
   }
    reverse(n.begin(), n.end());
    int nn=0,c=0;
    for(int i=0;i<n.size();i++,c++)
       nn=nn*10+n[i];
	int flipped=nn;
	int reverseFlipped=reverseNum(nn);
	
	if(prime(normal)==0) return 'C';
	else if(prime(normal)==1 && prime(reversed)==1 && prime(flipped)==1 && prime(reverseFlipped==1)) return 'B';
	else if(prime(normal)==1 && prime(reversed)==1 && prime(flipped)==0 && prime(reverseFlipped==0)) return 'E';
	else return 'P';
  
}
