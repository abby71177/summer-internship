//finding charcter frequency ,Checking Palindrome,removing vowels,find usingstring header file
#include<iostream>
#include<xstring>
#include<cctype>
#include<string>

bool checkPalindrome(std::string str) {
	std::string reverse("");
	for (int index = str.length()-1;index >= 0;index--)
		reverse += str[index];
	if (str == reverse)
		return true;
	else
		return false;
}

std::string removeVowels(std::string sentence) {
	std::string result;
	for (int index = 0;index < sentence.length();index++)
	{
		
			switch (sentence[index]) {
			case 'a': break;
			case 'e': break;
			case 'i': break;
			case 'o': break;
			case 'u': break;
			default: result += sentence[index]; break;

			}
	}
	return result;
}

void displayFrequency(std::string word) {
	int hash[26],index;
	char start = 'a',current;
	for (index = 0;index < 26;index++)
		hash[index] = 0;
	for (index = 0;index < word.length();index++)
		if (isalpha(word[index]))
		{
			int index1 = word[index] - start;
			hash[index1]++;
		}
	for (index = 0;index < 26;index++) {
		char current = start + index;
		std::cout << current << " : " << hash[index]<<std::endl;
	}
}

int main() {
	std::string str;
	std::cin >> str;
	if (checkPalindrome(str)) 
		std::cout << "entered string is a palindrome";
	else
		std::cout << "entered string is a palindrome";

	std::cout << "\nenter a sentence";
	std::getline(std::cin, str);
	//std::cout << str;
	std::size_t found = str.find("red");
	if (found != std::string::npos)
		std::cout << "'red'  found at: " << found << '\n';
	else
		std::cout << "not found in the sentence";
	std::cout << "\nSentence after removing the vowels is " << removeVowels(str);

	std::cout << "\nFrequency of characters in the sentence\n";
	displayFrequency(str);

	return 0;
	
}
