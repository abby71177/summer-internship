#include<iostream>
#include<string>
using namespace std;

template<typename X>               //templates are evaluated at COMPILE TIME!!!!!!
void fun(X a)
{
    cout << "\nValue of a is : " << a;
}
template<typename X, typename Y> 
void fun(X b, Y c)
{
    cout << "\nValue of b is : " << b;
    cout << "\nValue of c is : " << c;
}
template<typename Xx, typename Yy>

class Add {
    Xx num1;
    Yy num2;
public:
    Add(Xx a,Yy b) {
        num1 = a;
        num2 = b;
    }
     Xx addNumbers() { return num1 + num2; }

};
int main()
{
    fun(10);
    fun(20, 30.5);
    fun("Abby");
    Add<int,float> obj(10,5.5);
    cout << "\n\nresult ::"<<obj.addNumbers(); //returns int result as return type is int
    return 0;
}
