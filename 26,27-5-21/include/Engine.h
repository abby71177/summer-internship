#pragma once
#include<iostream>
#include<string>
#include<array>
#include<tuple>
using namespace std;
struct Engine {
	string name;
	string dept;
	int year;
	Engine():name("Abirami"),dept("IT"),year(3){}
	void getData();
	static array<string, 3> printMessage(Engine*);
	static tuple<string, string, string, int> printMessage(Engine*, const char*);

};

