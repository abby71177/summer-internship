#include<iostream>
#include<array>
using namespace std;
int binarySearch(array<int, 5> arr,int element) {
	int first, last, mid;
	first = 0;
	last = arr.size() - 1;
	mid = (first + last) / 2;
	while (first <= last) {
		if (arr[mid] == element)
			return mid + 1;
		else if (arr[mid] > element)
			last = mid - 1;
		else
			first = mid + 1;
		mid = (first + last) / 2;
	}
	if (first > last)
		return -1;
}
int main() {
	array<int, 5> arr = { 1, 2, 3, 4, 5 };

	for (auto element : arr)
		cout << element << ' ';
	//sort(arr, arr + n, greater<int>());
	cout << "\nElement 4 is at " << binarySearch(arr, 4);

	
}
