#include<iostream>
#include<string>
using String = std::string;


class Monopoly {
	//String playerName;
public:
	String playerName;
	Monopoly():playerName("Unknown"){}
	Monopoly(const char* Name) {
		playerName = Name;
	}
	const String& getName(){

		return playerName;
	}
};
int main() {
	int* arr = new int[10]; //new returns pointer to the memory allocated in heap
	int* ptr = new int;
	*ptr = 10;
	delete[] arr;
	delete ptr;

	Monopoly* obj = new Monopoly(); //calls the constructor as well
	//Monopoly* obj = new Monopoly[2]; 

	//Monopoly* ong = (Monopoly*)malloc(sizeof(Monopoly)); //does not call the constructor
	std::cout << (obj->getName)();;

	//Monopoly* obj1 = new Monopoly("Albert");

	delete obj;
	//std::cout << *ptr;

}
