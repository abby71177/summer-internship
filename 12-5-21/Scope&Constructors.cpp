#include<iostream>
class Master {
	const char* name;
public:
	Master(const char* Name):name(Name) {
		std::cout << "\nConstructor called for "<<this->name;
		
	}
	Master(Master &obj) :name(obj.name){
		std::cout << "\nCopy Constructor called for "<<this->name;
	}
	~Master() {
		std::cout << "\nDestructor called for "<<this->name;
	}
};
int main() {
	Master *object = new Master ("Ruther"); //destructor is not called 
	Master object1("Archie");
	Master object2 = object1;
	delete object;  //calls destructor for Ruther explicitly

}
