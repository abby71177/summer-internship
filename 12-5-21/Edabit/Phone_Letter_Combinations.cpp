/*
Phone Letter Combinations
Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

Alternative Text

Examples
letter_combinations("23") ➞ ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]

letter_combinations("532") ➞ ["jda", "jdb", "jdc", "jea", "jeb", "jec", "jfa", "jfb", "jfc", "kda", "kdb", "kdc", "kea", "keb", "kec", "kfa", "kfb", "kfc", "lda", "ldb", "ldc", "lea", "leb", "lec", "lfa", "lfb", "lfc"]
Notes
N/A

letterCombinations_
T1
Test Passed
T2
Test Passed
T3
Test Passed
T4
Test */
using namespace std;

vector<string> letterCombinations(string digits) {
    std::map<char,std::string> order{{'2', "abc"}, {'3', "def"}, {'4', "ghi"}, {'5', "jkl"}, {'6', "mno"}, {'7', "pqrs"}, {'8', "tuv"}, {'9', "wxyz"}};
    std::vector<std::string> result;
    char ch = digits[0];
    std::string m = order.at(ch);
    for(int index=0; index<m.length(); index++){
        std::string s = "";
        s += m[index];
        result.push_back(s);
    }
    for(int index=1; index<digits.length(); index++){
        std::vector<std::string> next;
        std::string m2 = order.at(digits[index]);
        for(int indexj=0; indexj<result.size(); indexj++){
            std::string a = result[indexj];
             for(int indexk=0; indexk<m2.length(); indexk++){
                std::string s = a;
                s += m2[indexk];
                next.push_back(s);
             }
        }
        result = next;
    }
    return result;
}
