/*
Same Letter Patterns
Same Letter Patterns
Create a function that returns true if two strings share the same letter pattern, and false otherwise.

Examples
sameLetterPattern("ABAB", "CDCD") ➞ true

sameLetterPattern("ABCBA", "BCDCB") ➞ true

sameLetterPattern("FFGG", "CDCD") ➞ false

sameLetterPattern("FFFF", "ABCD") ➞ false
Notes
N/A */
bool sameLetterPattern(std::string str1, std::string str2) {
	int index,flag=0,patternA,patternB;
	if(str1.length()!=str2.length())
		return false;
	else{
	for(index=1;index<str1.length() && index<str2.length();index++)
	{
		patternA=str1[index-1]-str1[index];
		patternB=str2[index-1]-str2[index];
		if(patternA==patternB)
			flag++;
	}
	//std::cout<<str1.length()<<str2.length()<<flag;
	if(flag==str1.length()-1) return true;
	else return false;
	}	
}
