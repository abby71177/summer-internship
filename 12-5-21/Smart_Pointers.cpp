#include<iostream>
class Master {
	const char* name;
public:
	Master(const char* Name):name(Name) {
		std::cout << "\nConstructor called for "<<this->name;
		
	}
	Master(Master &obj) :name(obj.name){
		std::cout << "\nCopy Constructor called for "<<this->name;
	}
	~Master() {
		std::cout << "\nDestructor called for "<<this->name;
	}
	void showStatus() {
		std::cout << this->name << " is Idle\n";
	}
};

class Slave {
	Master* obj;
public:
	Slave(Master *m):obj(m){}
	Master* operator->() {
		return obj;

	}
};
int main() {
	/*Master *object = new Master ("Ruther"); //destructor is not called 
	Master object1("Archie");
	Master object2 = object1;
	delete object;  //calls destructor for Ruther explicitly*/
	std::unique_ptr <Master> ptr (new Master("Abigail"));
	//std::unique_ptr <Master> ptr2 = ptr; // not allowed as per its copy constructor
	std::shared_ptr <Master> ptr2(new Master("Micheal"));
	std::shared_ptr <Master> ptr3 = ptr2; //constructor and destructor is not called
	ptr->showStatus();
	ptr2->showStatus();
	ptr3->showStatus();

	Slave s = new Master("Benjamin");
	s->showStatus();




} 
