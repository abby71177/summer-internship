#include<iostream>
#include<vector>
#include<string>

struct Date {
	int date, month, year;
	Date(int dd, int mm, int yyyy) :date(dd), month(mm), year(yyyy) { std::cout << "constructor called\n"; }
	Date(Date& other){
		std::cout << "copy constructor called\n";
	}
	
};
Date returnObj() {
	Date d(20, 12, 2021);
	return d;

}
void display(Date dates) {
	std::cout << dates.date << "-" << dates.month << "-" << dates.year << std::endl;
}

int main() {
	int i,j;
	//2s integer vector
	std::vector<std::vector<int>>arr;
	arr.push_back({ 10,20,30 });
	arr.push_back({ 40,50,60 });
	arr.push_back({ 70,80,90 });

	for ( i = 0;i < arr.size();i++) {
		std::cout << std::endl;
		for ( j = 0; j < arr[i].size();j++)
			std::cout << arr[i][j] << " ";
	}
	std::cout << std::endl;

	//2d userdefined vector
	std::vector<Date> dat;
	dat.push_back({ 12,5,2021 });
	dat.push_back({ 10,5,2021 });
	dat.emplace_back(5, 5, 2021); //directly passing parameters of the constructor
	dat.push_back(returnObj());
	//for (Date& d : dates)
	for (i = 0;i < dat.size();i++)
		display(dat[i]);

	//2d charcter vector
	std::vector<std::vector<std::string>>studentInfo;
	std::string name,dept,year;            
	for (i = 0;i < 5;i++)
	{
	std::cin >> name >> dept >> year;
	studentInfo.push_back({ name,dept,year });

	}
	for (i = 0;i < 5;i++)
		std::cout << studentInfo[i][0] << " " << studentInfo[i][1] << " " << studentInfo[i][1] << std::endl; 

	//Matrix Input
	std::vector<std::vector<int>>list(4);
	int value;
	for (i = 0;i < 4;i++) {
		for (j = 0;j < 4;j++) {
			std::cout << "enter element";
			std::cin >> value;
			list[i].push_back(value);
		}
	}
	for (i = 0;i < list.size();i++)
	{
		std::cout << std::endl;
		for (j = 0;j < list[i].size();j++)
			std::cout << list[i][j] << " ";
	}






}
