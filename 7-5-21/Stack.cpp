#include<iostream>
#include<string>
#include <cstring>

struct node{
char name[20];
struct node* next;
}*top=nullptr;

void push(char name1[20]){

struct node* newnode=(struct node *)malloc(sizeof(struct node));
strcpy(newnode->name,name1);
newnode->next=nullptr;
if(top==nullptr)
    top=newnode;
else
    {
        newnode->next=top;
        top=newnode;
    }

}

void display(){
struct node* temp=(struct node *)malloc(sizeof(struct node));
temp=top;
while(temp!=nullptr){
    std::cout<<temp->name<<"\n";
    temp=temp->next;
}

}

int main(){
    char choice,name[20];

    do{
        std::cout<<"Enter name to add into stack\n";
        std::cin>>name;
        push(name);
        std::cout<<"Do you wanna continue?(y/n)";
        std::cin>>choice;
    }while(choice=='y');

display();
return 0;
}
