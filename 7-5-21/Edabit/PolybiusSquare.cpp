/* Polybius Square (Basic)
The Polybius Square cipher is a simple substitution cipher that makes use of a 5x5 square grid. The letters A-Z are written into the grid, with "I" and "J" typically sharing a slot (as there are 26 letters and only 25 slots).

1	2	3	4	5
1	A	B	C	D	E
2	F	G	H	I/J	K
3	L	M	N	O	P
4	Q	R	S	T	U
5	V	W	X	Y	Z
To encipher a message, each letter is merely replaced by its row and column numbers in the grid.

Create a function that takes a plaintext or ciphertext message, and returns the corresponding ciphertext or plaintext.

Examples
polybius("Hi") ➞ "2324"

polybius("2324  4423154215") ➞ "hi there"

polybius("543445 14343344 522433 21422415331443 52244423 4311311114") ➞ "you dont win  friends with salad" */


std::string polybius(std::string text) {
	std::string result ("");
	char a[][5]={{'A','B','C','D','E'},{'F','G','H','I','K'},{'L','M','N','O','P'},{'Q','R','S','T','U'},{'W','X','Y','Z'}};
	for(int i=0;i<text.length();i++){
		if(isalpha(text[i])){
			for(int i=0;i<5;i++){
				for(int j=0;j<5;j++)
					if(a[i][j]==text[i]){result+=i+1;
															result+=j+1;}
				  i(text[i]=='J'){result+='2';
															result+='3';}
			}
			
		}
		else if(isdigit(text[i])){
			result+=a[i][i+1];
			i++;
		}
		else 
			result+=text[i];
	}
}
