#include<iostream>
#include<vector>
#include<string>
#include<map>
#include <utility> 
#include<array>


using namespace std;
template<typename X>
class Entity {
	X name;
public:
	Entity(X variable):name(variable){}
	void displayName() {
		cout << this->name;
	}

};
void function(){
	cout << "function called by a fn pointer";
}

void function2(void(*ptr)()) {
	ptr();
}

void display(array <int, 5>& arr, void(*ptr)(int x)) {
	for (auto iter=arr.begin();iter!=arr.end();iter++)
		ptr(*iter);
}

void display(vector<string>& arr, void(*ptr)(string x)) {
	for (auto iter = arr.begin();iter != arr.end();iter++)
		ptr(*iter);

}

class Log {
	string logId;
public:
	Log(string name):logId(name){}

};

class logInfo {
	int count;
public:
	logInfo(int c):count(c){}
	void display() {
		cout << this->count<<endl;
	}
};
typedef vector<Log> logVector;
typedef map<int, logInfo* > logDetails;
int main() {
	Log obj("abc");
	logDetails log;
	Log obj1("xyz");
	
	//log[1] = &i;
	//auto res = log[1];
	//res->display();
	for (int i = 0;i < 5;i++)
	{
		logInfo I(i*i);
		log[i] = &I;

	}
	for (auto iter = log.begin(); iter != log.end(); ++iter)
	{
		auto res = *iter;
		cout << res.first<<"->";
		res.second->display();
		

	}
/*
	vector<int> a;
	a.push_back(9);
	a.push_back(98);
	a.push_back(97);
	for (auto index = a.begin();index != a.end();index++)
		cout << *index << endl;*/
	Entity<int> e(10);
	//e.displayName();
	void (*Ptr) ();
	Ptr = function;
	function2(function);
	Ptr();
	cout << endl << endl << endl;
	array<int, 5>arr= { 1,2,3,4,5 };
	vector <string> names = { "abc","def","ghi","hij" };
	display(names, [](string value) {cout << value << endl;});


	

	











}
