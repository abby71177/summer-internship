#include<iostream>
struct Union {
	union {
		int a;
		float b;

	};
};


struct Coords {
	float x, y;
};

void display(Coords c) {
	cout << "\nCords\n" << c.x << " , "<<c.y;
}

struct _Coords {

	// instead of return *(coords*)&x; 
	union {
		struct {
			float x1, y1, x2, y2;

		};
		struct {
			Coords a, b;
		};
	};
};


int main(){
    int i = 10;
	char name = 'a';
	double I = *(double*)&i;
	int _name = *(int*)&name;

	cout << "type punning " << _name;
		
	cout << "\n\nunions\n";
	Union obj;
	obj.a = 10;
	cout << obj.a << "  "<<obj.b;
	cout << "\nImplicit type punning\n";
	_Coords coords = { 2.2f,3.3f,4.4f,5.5f };
	display(coords.a);
	display(coords.b);
	coords.x1 = 9.9f;
	coords.x2 = 9.9f;
	coords.y1 = 9.9f;
	coords.y2 = 9.9f;
	display(coords.a);
	display(coords.b);
}
