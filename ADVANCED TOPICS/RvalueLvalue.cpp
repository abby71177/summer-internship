#include<iostream>
#include<string>
using namespace std;

/*
& for lvalue
&& for rvalue
const for both

*/

void display(int&& id, string name) {
	cout << endl;
	cout << "rvalue\n";
	cout << "Id->" << id << "  Name->" << name << endl;
}
void display( int& id, string name) {
	cout << endl;
	cout << "lvalue\n";

	cout << "Id->" << id << "  Name->" << name << endl;
}
/*
void display(const int id, string name) {
	cout << endl;
	cout << "default\n";

	cout << "Id->" << id << "  Name->" << name << endl;
}*/


int main() {
	int i = 5007;
	string name = "abbi";
	display(i, "abirami");
	//display(5020, "Aravind" + ".s");
	display(5020, "Aravind");
}
