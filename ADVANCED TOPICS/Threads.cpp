
#include<iostream>
#include<thread>
#include <chrono>      

using namespace std;

void Thread(int s) {
	cout << "\nthread working....\n";
	cout << "sleeping for " << s << " seconds....\n";
	this_thread::sleep_for(chrono::seconds(s));
}

int main() {
	thread workerA(Thread,5);
	thread workers[2];
	thread workerB(Thread,5);

	workers[0] = thread(Thread, 5);
	workers[1] = thread(Thread, 5);

	cout<<workerA.get_id();
	workerA.join();

	cout << workerB.get_id();
	workerB.join();

	workers[0].join();
	workers[1].join();
	
	//cout << workerA.get_id();
	//workerA.detach();
	//cout << workerB.get_id();
	//workerB.detach();




}
