
#include<string>
#include<iostream>
#include<string_view>
#include<memory>
using namespace std;


#include"pch.h"
struct Union{
	union {
		int a, b;
		float c;
		float d;
	};
	
};
struct memory {
	size_t allocated, free;
	memory():allocated(0),free(0){}
}_mem;

void display(string_view name) {
	cout << name;
}

void* operator new(size_t size)
{
	cout << "Allocated ...." << size << endl;
	_mem.allocated += size;
	return malloc(size);
}
void operator delete( void*memory,size_t size)
{
	cout << "Deleted ...." << size << endl;
	_mem.free += size;
	free(memory);
}
void memoryUsage() {
	cout << "Allocated memory->" << _mem.allocated << endl;
	cout << "Deleted memory->" << _mem.free;
}

int main() {

	//string _name = "Abirami";
	//16 characters 32 bytes of memory on heap
	Union* _u = new Union();
	delete _u;

	memoryUsage();
	cin.get();

}

