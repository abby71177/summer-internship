#include<iostream>
using namespace std;

class Master {
	string id, name;

public:
	Master():id("M"),name("Master"){}
	virtual void display() {
		cout << "\nId->" << id << "\nName->" << name << endl;

	}
};

class Slave:public Master{
	string id, name;
public:
	Slave() :id("S"), name("Slave") {}
	void display() {
		cout << "\nId->" << id << "\nName->" << name << endl;

	}

};
class Bug:public Master{
	string id, name;
public:
	Bug() :id("B"), name("Bug") {}
	void display() {
		cout << "\nId->" << id << "\nName->" << name << endl;

	}

};



class classA {
	string id, name;
public:
	classA() :id("A"), name("CLASS-A"){}
};

class classB {
	string id, name;
public:
	classB() :id("B"), name("CLASS-B") {}

	void display() {
		cout << "\nId->" << id << "\nName->" << name << endl;
	}
};
int main() {
	//implicit
	double b = 2.4500;
	int a = b;
	cout << a << endl;

	//explicit
	float f = 2.4567f;
	int _f = int(f);
	//int _f=static_cast<int>(f);
	cout << _f;

	classB* _objB = new classB();
	_objB->display();

	classA _objA;
	_objB = (classB*) &_objA;
	_objB->display();


	//dynamic_cast (only when base class is polymorphic ie has a virtual fn
	Master* _m=new Master();
	Slave* _s=new Slave();
	Bug* _b=new Bug();
	_m = dynamic_cast<Master*> (_s);
	if (_m == 0)cout << _m;
	_m->display();

	_m = dynamic_cast<Master*> (_b);
	if (_m == 0)cout << _m;
	_m->display();

	Master master;
	_m = dynamic_cast<Master*> (&master);
	if (_m == 0)cout << _m;
	_m->display();


	//static_cast  (no safety check)

	cout << "\n\nStatic cast\n";
	Master* _M = new Master();
	Slave s;
	_M = static_cast<Slave*>(&s);
	//_M = static_cast<Master*>(&s);
	_M->display();
	Slave* _S = new Slave();
	//_S = static_cast<Slave>(&master); //cannot be done
	//delete _S;
	//delete _M;
	//delete _m, _s, _b;

	
	//reinterpret cast
	//in case of no inheritance
	cout << endl << endl << "Reinterpret cast\n";
	Bug* _B = new Bug();
	Slave* _ss = new Slave();

	_ss = reinterpret_cast<Slave*>(_B);
	_ss->display();
	Slave* _ss2 = new Slave();

	_B = reinterpret_cast<Bug*>(_ss2);
	_B->display();






	

}
