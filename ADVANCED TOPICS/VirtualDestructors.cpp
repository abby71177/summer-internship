#include<iostream>
using namespace std;
class Parent {
public:
	Parent() {
		cout << "\nBase Constructor";
	}
	 virtual ~Parent() {
		cout << "\nBase Destructor";
	}
};

class Child :public Parent {
public:
	Child() {
		cout << "\nChild Constructor";
	}
	~Child() {
		cout << "\nChild Destructor";
	}
};

int main() {
	Child* obj = new Child();
	delete obj;
	cout << endl << endl;
	Parent* master = new Child();
	delete master;

	//Child slave;
	//master = &slave;
}
