#include"pch.h"

optional<string> returnData(string name) {
	if (name == "abbc")
		return "true";
	else
		return {};

}
int main() {
	optional<string>result = returnData("abbbc");
	if (result.has_value())
		cout << "Result->" << result.value();
	else
		cout << "Invalid name";
	cout << endl << "Variant in C++\n";
	variant<string, int, float> _result;
	_result = 2.2f;
	cout << get<float>(_result);

	_result = "Zane";
	cout << get<string>(_result);

	_result = 2;
	cout << endl << "Any keyword";
	any _Result;
	_Result = 9.9f;
	_Result = "abby";


	float n = any_cast<float>(_Result);
	cout << endl << n;



}
