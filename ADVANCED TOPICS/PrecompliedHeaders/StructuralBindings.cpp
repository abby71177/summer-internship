#include"pch.h"

tuple<string, string, int> returnStudent() {
	return make_tuple("abi", "it", 007);
}
pair<string, string> returnStudent2() {
	return make_pair( "abc","cse" );
}

int main() {
	//cout << "helloo";t
	tuple<string, string, int> student = returnStudent();
	cout << get<0>(student) << endl;
	cout << get<1>(student) << endl;
	cout << get<2>(student) << endl;

	auto [name, dept, reg] = returnStudent();
	cout << name << "-->" << dept << "-->" << reg << endl;

	pair<string, string> student2 = returnStudent2();
	cout << endl << student2.first << student2.second << endl;
	auto [_name, _dept] = returnStudent2();
	cout << _name << "-->" << _dept << "-->"<< endl;
}
