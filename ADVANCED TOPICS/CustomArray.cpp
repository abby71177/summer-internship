#include<iostream>
#include<string>
using namespace std;
template<typename T,size_t S>
class Array {
	T arr[S];
public:
	 constexpr int retSize () const{
		return S;
	}

	T& operator[](int index) {
		return arr[index];
	}
};

int main() {
	constexpr int N = 4;
	Array<int, 3> data;
	cout << data.retSize();
	//memset(&data[0], 0, data.retSize() * sizeof(int));
	for (int i = 0;i < data.retSize();i++)
		data[i] = 5;
	for (int i = 0;i < data.retSize();i++)
		cout << data[i] << endl;

}
