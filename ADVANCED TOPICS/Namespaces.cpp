#include<iostream>
using namespace std;
namespace ENTITY {
	namespace methods {
		int id;
		void assign(int n) {
			id = n;
			cout << "assigned";
		}
		void display() {
			cout << "ID->" << id;
		}
		
		void reassign();
	}
	void display() {
		cout << "entity details";
	}
}
void ENTITY::methods::reassign() {
	cout << "reassigned!!";
}

namespace LOG {
	void assign() {
		cout << "\nassigned";
	}
	void display() {
		cout << "\nlog displayed";
	}
}

void functionA(int number) {
	cout << "called"<<number;
}

void functionB(void (*ptr)(int n))
{
	ptr = functionA;
	ptr(1000);
}


int main(){
    using namespace ENTITY;
    display();
    namespace e=ENTITY::methods;
    e::assign();
    e::display();
    LOG::assign();
    void (*PTR)(int);
    PTR=functionA;
    PTR(1000);


}
