#include<iostream>
#include<string>
#include<tuple>
//singleton is where youd create only once instance of an object 
class SingletonA {
	string name, id;
	SingletonA():name("ABC"),id("00001"){}
	static SingletonA _instance;
public:
	static SingletonA& getInstance() {
		return _instance;
	}
	tuple<string, string> data() {
		return make_tuple(id, name);
	}
};

SingletonA SingletonA::_instance;

int main(){
    auto[id,name]=SingletonA::getInstance().data();
    cout<<"Id->"<<id<<"   Name->"<<name;
}


