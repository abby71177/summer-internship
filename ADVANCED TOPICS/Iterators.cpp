#include<iostream>
#include<vector>
#include<map>
#include<unordered_map>
#include<string>
using namespace std;

int main() {
	unordered_map<int, int> data;
	data[2] = 90;
	data[5] = 67;
	data[12] = 43;
	data[3] = 5;
	data[7] = 89;
	data[5] = 23;
	string name = "abby";
	string name2 = "ben";
	cout << "unordered map\n";
	for (auto itr : data)
		cout << itr.second << "  ";

	cout << "\nMap\n";
	map<int, string>_Data;
	_Data.insert({ 2,"Reg-2" });
	_Data.insert({ 5,"Reg-5" });
	_Data.insert({6 ,"Reg-6" });
	_Data.insert({ 12,"Reg-12" });
	_Data.insert({ 9,"Reg-9" });
	for (auto i : _Data)
		cout << i.second << "  ";

	cout << "\nVector\n";
	vector<int> _data = { 4,90,12,45,1,768,67 };
	sort(_data.begin(), _data.end(), greater<int>());
	for (auto incr = _data.begin();incr != _data.end();incr++)
		cout << *incr << "  ";

	
	
	
}
