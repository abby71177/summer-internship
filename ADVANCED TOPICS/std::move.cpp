#include<iostream>
#include<string>
#include<vector>
using namespace std;

template <typename T>
class Master {
	T id;
	vector<T> info;
public:
	Master() :id("Unknown"), info ({ "null","null" }) {}
	Master(const T _id,const vector<T>_info) :id(_id) ,info(_info) {}

	Master(Master&& _m ) {
		id = move(_m.id);
		info = move(_m.info);
	}
	void display() {
		cout << id << endl;
		for (auto i : info)
			cout << i << " ";
		cout << endl;
	}
};

int main(){
    string name1 = "Singleton";
	string name3 = move(name1);
	name3 += " C++";
	name3 = move("Hello");
	cout <<endl<< name1 << endl << name3;
	vector<string> nameInfo = { "a","b","c" };
	Master<string> m("Abby",nameInfo);
	cout << "M->";
	m.display();
	Master<string>m1 = move(m);   //cals copy constructor
	cout << "M1->";
	m1.display();
}

