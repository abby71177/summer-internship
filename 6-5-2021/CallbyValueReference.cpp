#include<iostream>
void byValue( int a,int b){
  a*=a;
  b*=b;
  std::cout<<"\nInside Call By Value function\nA= "<<a<<" B= "<<b;
}

void byReference(int *a,int *b){
  *a*=*a;
  *b*=*b;
  std::cout<<"\nInside Call By Reference function\nA= "<<*a<<" B= "<<*b;
}

int main(){
int num1,num2;
num1=10;
num2=10;
std::cout<<"Values before call by value\n";
std::cout<<"A= "<<num1<<" B= "<<num2;
byValue(num1,num2);
std::cout<<"\nValues after call by value\n";
std::cout<<"A= "<<num1<<" B= "<<num2;
std::cout<<"\nValues before call by Reference\n";
std::cout<<"A= "<<num1<<" B= "<<num2;
byReference(&num1,&num2);
std::cout<<"\nValues after call by Reference\n";
std::cout<<"A= "<<num1<<" B= "<<num2;
return 0;

}
