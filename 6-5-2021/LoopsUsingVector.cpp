#include <iostream>
#include <vector>

int BinarySearch(std::vector <int> arr,int element){
int first=0,last,mid;
last=arr.size()-1;
mid=(first+last)/2;

while(first<last){
    if(arr[mid]==element)
        return mid-1;
    else if(arr[mid]>element)
        last=mid-1;
    else
        first=mid+1;
    mid=(first+last)/2;

}
if (first>last)
    return -1;



}

int main() {
    int no,sum=0;
    char choice;
    std::vector <int> vec;
 do{
    std::cout<<"Enter number to insert: ";
    std::cin>>no;
    vec.push_back(no);
    std::cout<<"Do you wanna continue?(y/n):";
    std::cin>>choice;
 }while(choice=='y');

 for(int i=0;i<vec.size();i++)
    sum+=vec[i];
 std::cout<<"\n Sum of elements in the vector "<<sum;
 std::cout<<"Enter number to find in array: ";
 std::cin>>no;
 int result=BinarySearch(vec,no);
 if (result==-1)
   std::cout<<"\nElement not present in the array\n ";
 else
    std::cout<<"\nElement at position "<<result;
 return 0;
}
