/* Count Letters in a Word Search Level-Medium
Create a function that counts the number of times a particular letter shows up in the word search.
Examples

letterCounter([
  ["D", "E", "Y", "H", "A", "D"],
  ["C", "B", "Z", "Y", "J", "K"],
  ["D", "B", "C", "A", "M", "N"],
  ["F", "G", "G", "R", "S", "R"],
  ["V", "X", "H", "A", "S", "S"]
], "D") ➞ 3

// "D" shows up 3 times: twice in the first row, once in the third row.

letterCounter([
  ["D", "E", "Y", "H", "A", "D"],
  ["C", "B", "Z", "Y", "J", "K"],
  ["D", "B", "C", "A", "M", "N"],
  ["F", "G", "G", "R", "S", "R"],
  ["V", "X", "H", "A", "S", "S"]
], "H") ➞ 2   */


int letterCounter(vector<vector<char>> arr, char c) {
    int count=0;
    for(int i=0;i<arr.size();i++){
        for(int j=0;j<arr[i].size();j++)
        if(arr[i][j]==c)
          count++;
    }
    return count;
}
