/* Phone Number Word Decoder
Create a program that converts a phone number with letters to one with only numbers.

Number	Letter
0	none
1	none
2	ABC
3	DEF
4	GHI
5	JKL
6	MNO
7	PQRS
8	TUV
9	WXYZ
Examples
textToNum("123-647-EYES") ➞ "123-647-3937"

textToNum("(325)444-TEST") ➞ "(325)444-8378"

textToNum("653-TRY-THIS") ➞ "653-879-8447"

textToNum("435-224-7613") ➞ "435-224-7613" */




std::string textToNum(std::string phone) {
	std::string result("");
	for(int i=0;i<phone.length();i++)
		switch(phone[i]){
			case 'A': result+='2'; break;
			case 'B': result+='2'; break;
			case 'C': result+='2'; break;
			case 'D': result+='3'; break;
			case 'E': result+='3'; break;
			case 'F': result+='3'; break;
			case 'G': result+='4'; break;
			case 'H': result+='4'; break;
			case 'I': result+='4'; break;
			case 'J': result+='5'; break;
			case 'K': result+='5'; break;
			case 'L': result+='5'; break;
			case 'M': result+='6';break;
			case 'N': result+='6'; break;
			case 'O': result+='6'; break;
			case 'P': result+='7'; break;
			case 'Q': result+='7'; break;
			case 'R': result+='7'; break;
			case 'S': result+='7'; break;
			case 'T': result+='8'; break;
			case 'U': result+='8'; break;
			case 'V': result+='8'; break;
			case 'W': result+='9'; break;
			case 'X': result+='9'; break;
			case 'Y': result+='9';break;
			case 'Z': result+='9'; break;
			default: result+=phone[i]; break;
				
		}
	return result;
}
