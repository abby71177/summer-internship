/* To Arrange Elements in Ascending Order- level MEADIUM
Using vectors



Create a function that takes an array of numbers and returns a new array, sorted in ascending order (smallest to biggest).

    Sort numbers array in ascending order.
    If the function's argument is null, an empty array, or undefined; return an empty array.
    Return a new array of sorted numbers.

Examples

sortNumsAscending([1, 2, 10, 50, 5]) ➞ [1, 2, 5, 10, 50]

sortNumsAscending([80, 29, 4, -95, -24, 85]) ➞ [-95, -24, 4, 29, 80, 85]

sortNumsAscending([]) ➞ [] */


std::vector <int> AscendingOrder(std::vector<int> arr){
    vector<int> myvector{};
    if(arr.empty()){
        return myvector;
    }
    else{
    int position,temp;
    for(int i=0;i<arr.size();i++){
        temp=arr[i];
        position=i;
        for(int j=i+1;j<arr.size();j++){
            if(temp>arr[j]){
                position=j;
                temp=arr[j];
            }
        }
        temp=arr[i];
        arr[i]=arr[position];
        arr[position]=temp;
    }
    return arr;
    }
}
