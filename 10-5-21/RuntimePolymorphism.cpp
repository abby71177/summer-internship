#include<iostream>
#include<string>

class Master {
	std::string  name;
public:
	
	Master() {
		name = "Master-Default";
	}
	Master(std::string name1) {
		
		name = name1;
	}
	virtual void functionA() {
		std::cout << name << " is performing task A\n";
	}
	virtual void functionB() {
		std::cout << name << " is performing task B\n";
	}
	void functionC() {
		std::cout << name << " is performing task C\n";
	}

};
class Slave :public Master {
	std::string  name;
public:
	Slave() {
		name = "Slave-Default";
	}
	Slave(std::string name1) {
		Master("Master-ABC");
		name = name1;
	}
	void functionA() {
		std::cout << name << " is performing task A\n";
	}
	void functionB() {
		std::cout << name << " is performing task B\n";
	}
	void functionC() {
		std::cout << name << " is performing task C\n";
	}
	void functionD() {
		std::cout << name << " is performing task D\n";
	}


};
int main() {
	Slave *slaveObj=new Slave("Slave-XYZ");
	Master *masterObj;
	Slave slaveObj1;
	 masterObj= &slaveObj1;
	slaveObj->functionA();  //Slave methods only get executed
	slaveObj->functionB();
	slaveObj->functionC();
	slaveObj->functionD();

	masterObj->functionA();  //virtual function functionA() is hidden at runtime so Slave A() gets executed
	masterObj->functionB();
	masterObj->functionC();   //not a virtaul function so Master C() gets executed
	//masterObj->functionD();

}
