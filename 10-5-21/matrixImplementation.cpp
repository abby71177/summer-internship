#include<iostream>
#include<vector>
#include "Matrix.h"

int main() {
	std::vector<std::vector<int>> matrix1,matrix2,matrix3;
	Matrix object;
	matrix1=object.getMatrix();
	matrix2 = object.getMatrix();
	std::cout << "\nMatrix 1\n";
	object.displayMatrix(matrix1);

	std::cout << "\nMatrix 2\n";
	object.displayMatrix(matrix2);

	matrix3 = addMatrices(matrix1, matrix2);
	std::cout << "\nMatrix Obtained After Addition\n";
	object.displayMatrix(matrix3);

	matrix3 = subtractMatrices(matrix1, matrix2);
	std::cout << "\nMatrix Obtained After Subtraction	\n";
	object.displayMatrix(matrix3);

	matrix3 = multiplyMatrices(matrix1, matrix2);
	std::cout << "\nMatrix Obtained After Multiplication	\n";
	object.displayMatrix(matrix3);

	std::cout << "\nDiagonal Elements of Matrix 1\n";
	object.displayDiagonalElements(matrix1);
	std::cout << "\nDiagonal Elements of Matrix 2\n";
	object.displayDiagonalElements(matrix2);






	return 0;



}
