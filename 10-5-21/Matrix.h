#pragma once
#include<iostream>
#include<vector>
class Matrix {
	
public:
	Matrix();
	std::vector<std::vector<int>> getMatrix();
	void setMatrix();
	std::vector<std::vector<int>> addMatrices(std::vector<std::vector<int>>, std::vector<std::vector<int>>);
	 std::vector<std::vector<int>> subtractMatrices(std::vector<std::vector<int>>, std::vector<std::vector<int>>);
    std::vector<std::vector<int>> multiplyMatrices(std::vector<std::vector<int>>, std::vector<std::vector<int>>);
	void displayDiagonalElements(std::vector<std::vector<int>>);
	void transposeMatrix(std::vector<std::vector<int>>);
	void displayMatrix(std::vector<std::vector<int>>);

};
Matrix::Matrix() {

}

std::vector<std::vector<int>>addMatrices(std::vector<std::vector<int>> matrix1, std::vector<std::vector<int>> matrix2) {
	int row1, col1, row2, col2;
	
	row1 = matrix1.size();
	col1 = matrix1[0].size();
	row2 = matrix2.size();
	col2 = matrix2[0].size();
	std::vector< std::vector< int > >matrix3(row1);
	if (row1 == row2 && col1 == col2) {
		
		for (int i = 0; i < row1; i++) {
			for (int j = 0; j < col1; j++)
				matrix3[i].push_back(matrix1[i][j] + matrix2[i][j]);
		}
		//std::cout << "Resultant Matrix\n";
		return matrix3;


	}
	else {
		std::cout << "Given Matrices canot be added" << std::endl;
		//return matrix3 ;
	}

}

std::vector<std::vector<int>>subtractMatrices(std::vector<std::vector<int>> matrix1, std::vector<std::vector<int>> matrix2) {
	int row1, col1, row2, col2;
	row1 = matrix1.size();
	col1 = matrix1[0].size();
	row2 = matrix2.size();
	col2 = matrix2[0].size();
	std::vector< std::vector< int > >matrix3(row1);
	if (row1 == row2 && col1 == col2) {
		std::vector< std::vector< int > >matrix3(row1);
		for (int i = 0; i < row1; i++) {
			for (int j = 0; j < col1; j++)
				matrix3[i].push_back(matrix1[i][j] - matrix2[i][j]);
		}
		//std::cout << "Resultant Matrix\n";
		return matrix3;


	}
	else {
		std::cout << "Given Matrices cannot be subtracted" << std::endl;
		
	}

}
std::vector<std::vector<int>>multiplyMatrices(std::vector<std::vector<int>> matrix1, std::vector<std::vector<int>> matrix2) {
	int row1, col1, row2, col2;
	row1 = matrix1.size();
	col1 = matrix1[0].size();
	row2 = matrix2.size();
	col2 = matrix2[0].size();
	if (col1==row2) {
		std::vector< std::vector< int > >matrix3(row1);
		for (int index1 = 0; index1 < row1; index1++) {
			int value = 0;
			for (int index2 = 0; index2 < col2; index2++)
				for (int index3 = 0; index3 < col1; index3++)
					value += matrix1[index1][index3] * matrix2[index3][index1];
			    matrix3[index1].push_back(value);
		}
		//std::cout << "Resultant Matrix\n";
		return matrix3;


	}
	else {
		std::cout << "Given Matrices cannot be Multiplied" << std::endl;
		//return;
	}

}
std::vector<std::vector<int>> Matrix::getMatrix() {
	int value,rows,cols;
	std::cout << "enter nomber of rows and columns"<<std::endl;
	std::cin >> rows >> cols;
	std::cout << "Enter the elements of the Matrix" << std::endl;
	std::vector< std::vector< int > > myvector(rows);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			std::cin >> value;
			myvector[i].push_back(value);
		}
	}
	return myvector;


}
void Matrix::displayMatrix(std::vector<std::vector<int>> arr) {
	
	for (int index1 = 0;index1 < arr.size();index1++) {
		std::cout << std::endl;
		for(int index2 = 0;index2 < arr[0].size();index2++) {
			std::cout << arr[index1][index2] << " ";
		}
	}

}

void Matrix::displayDiagonalElements(std::vector<std::vector<int>> arr) {
	for (int index1 = 0;index1 < arr.size();index1++) {
		std::cout << std::endl;
		for (int index2 = 0;index2 < arr[0].size();index2++) {
			if (index1 == index2 || index1 == (arr.size() - index2 - 1))
				std::cout << arr[index1][index2] << " ";
			else
				std::cout << "  ";
		}
	}

}
