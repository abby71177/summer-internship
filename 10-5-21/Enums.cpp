#include<iostream>
//enums by default are 32 bit
enum Vowels {
	a=10,e,i,o=10,u
};
//cannot take float only int,unsigned int,signed int,char
enum Area :char {
	square='S',circle='C',rectangle='R',triangle='T'
};
enum Days :char {
	//Sun='S',Mon='M',Tue='T',Wed='W',Thirs='T',Fri='F',Sat='S'
	Sum,Mon,Tues,Wed,Thurs,Fri,Sat
};

int main() {
	Vowels value = u;
	std::cout << value; //returns the index of the arrangement a,e,i,o,u, returns 14 for a=10,e,i,o,u
	value = a;
	std::cout << value;//returns 10 as a=10
	Area shape = circle;
	std::cout << shape; 
	std::cout << std::endl;
	
	for (Days iCol = Mon; iCol != Sat; iCol=(Days(iCol+1)))
		std::cout <<iCol<< std::endl;
	return 0;



}
