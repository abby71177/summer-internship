#include<iostream>
#include<string>
class Student {
	std::string studentName;
	static int id;


public:
	
	Student() {
		studentName = "Benjamin";
		id = 567;

		std::cout << "\nConstructor\n";
	}
	 static void Display() {
		std::cout << id;
		//std::cout << Student::studentName;

	}
	~Student() {
		std::cout << "Destructor\n";

	}
};
int Student::id = 123;



int main() {
	static Student obj;
	obj.Display();
	
	std::cout << "\nMain function\n";

	return 0;
}
