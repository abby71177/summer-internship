 #include<iostream>
class Class{
	const char* classname;

public:
	Class() {
		classname = "Parent";
		
	}
	virtual void displayName() {
		std::cout << classname;
	}
};

class Member:public Class {
	const char* classname;

public:
	Member() {
		classname = "Child";

	}
	void displayName() override {
		
		std::cout << classname;
	}
};
void DisplayName(Class* c) {
	c->displayName();
}
int main() {
	Class* object = new Class();;
	object->displayName();
	Member* memberObject = new Member();
	memberObject->displayName();
	DisplayName(object);
	DisplayName(memberObject); //displays parent is the base class fn is not declared as virtual


	return 0;



}
