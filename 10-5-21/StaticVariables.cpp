
#include "Header.h"
/*
static class Log {
	int x, y;
public:
	Log() {
		x = 10;
		y = 10;
	}
	void displayXY() {
		std::cout << x << " " << y;
	}
}; */   //will throwerror as 2 instances of same class cannot be created


static void displayNumber() {
	static int number = 0;  //same as declaring a global variable
	std::cout << ++number;
}

int main() {
	for(int i=0;i<5;i++)
		displayNumber();
	Log object;
	object.displayXY();

	return 0;
}
