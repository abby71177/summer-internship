/* Sum of Digits Between Two Numbers
  Create a function that sums the total number of digits between two numbers, inclusive. For example, between the numbers 19 and 22 we have:

 19, 20, 21, 22
(1 + 9) + (2 + 0) + (2 + 1) + (2 + 2) = 19

Examples

sumDigits(7, 8) ➞ 15

sumDigits(17, 20) ➞ 29

sumDigits(10, 12) ➞ 6  */

#include<iostream>

int findSum(int num){
	int sum=0;
	while(num!=0){
		sum+=(num%10);
		num/=10;
	}
	return sum;
}
int sumDigits(int a, int b) {
	int total=0;
	for(int i=a;i<=b;i++)
		total+=findSum(i);
	return total;
	
}

int main(){
    std::cout<<sumDigits(19,22);
    return 0;
}
