/*  Flip the Boolean
Create a function that reverses a boolean value.
Examples

reverse(true) ➞ false

reverse(false) ➞ true */


bool reverse(bool boolean) {
	return ((boolean)?false:true);
}
