#include<iostream>
using namespace std;

template<typename T>
struct Node {
	T data;
	struct Node* next;
};

template <typename T>
class Stack {
	struct Node<T>* top = nullptr;
public:
	void push(int e) {
		//struct Node* newnode = (struct Node*)malloc(sizeof(struct Node));
		struct Node <T>* newnode = new Node<T>;
		newnode->data = e;
		newnode->next = NULL;
		if (top == NULL)
			top = newnode;
		else {
			newnode->next = top;
			top = newnode;

		}
	}
	void pop() {
		if (top == NULL)
			cout << "Stack is empty";
		else {
			cout << top->data << " is popped" << endl;;
			top = top->next;
		}
	}
	void display() {
		//struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
		struct Node<T>* temp = new Node<T>;
		temp = top;
		while (temp != NULL) {
			cout << temp->data << endl;
			temp = temp->next;


		}
		cout << endl;
	}
};

void main() {
	Stack<int> obj;
	obj.push(10);
	obj.push(20);
	obj.push(30);
	obj.push(40);
	obj.display();
	obj.pop();
	obj.push(60);
	obj.pop();
	obj.display();



}
