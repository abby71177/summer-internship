
#include<iostream>
#include<string>
using namespace std;
template <typename T>
struct Node {
	T element;
	struct Node* next;
};

template <typename T>
class Queue {
	Node<T>* front;
	Node<T>* rear;
public:
	Queue():front(nullptr),rear(nullptr){}
	void enqueue(T data) {
		Node <T>* newnode = new Node<T>;
		newnode->element = data;
		newnode->next = nullptr;
		if (front == nullptr && rear == nullptr)
		{
			front = newnode;
			rear = newnode;
		}
		else {
			rear->next = newnode;
			rear = newnode;
		}
	}

	void dequeue() {
		if (front == nullptr)
			cout << "empty queue";
		else {
			cout << front->element << " is deleted\n";
			front = front->next;
		}
	}

	void display() {
		Node<T>* temp = front;
		while (temp != NULL) {
			cout << temp->element << endl;
			temp = temp->next;
		}
	}
};

int main() {
	Queue<string> obj;
	obj.enqueue("abc");
	obj.enqueue("def");
	obj.enqueue("hij");
	obj.display();
	obj.dequeue();
	obj.display();
}
