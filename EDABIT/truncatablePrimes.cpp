/*
Truncatable Primes
A left-truncatable prime is a prime number that contains no 0 digits and, when the first digit is successively removed, the result is always prime.

A right-truncatable prime is a prime number that contains no 0 digits and, when the last digit is successively removed, the result is always prime.

Create a function that takes an integer as an argument and:

If the integer is only a left-truncatable prime, return "left".
If the integer is only a right-truncatable prime, return "right".
If the integer is both, return "both".
Otherwise, return "none".
Examples
truncatable(9137) ➞ "left"
// Because 9137, 137, 37 and 7 are all prime.

truncatable(5939) ➞ "right"
// Because 5939, 593, 59 and 5 are all prime.

truncatable(317) ➞ "both"
// Because 317, 17 and 7 are all prime and 317, 31 and 3 are all prime.

truncatable(5) ➞ "both"
// The trivial case of single-digit primes is treated as truncatable from both directions.

truncatable(139) ➞ "none"
// 1 and 9 are non-prime, so 139 cannot be truncatable from either direction.

truncatable(103) ➞ "none"
// Because it contains a 0 digit (even though 103 and 3 are primes). */

#include <vector>
bool containsZero(int num){
    int rem;
    while(num!=0){
        rem=num%10;
        if(rem==0)
          return true;
        num=num/10;
          
    }
    return false;
}
int noOfDigits(int num){
	int countt=0;
	while(num!=0){
		num=num/10;
		countt++;
	}
	return countt;
}
bool isPrime(int num){
	int flag=1;
	for(int i=2;i<num;i++){
		if(num%i==0) flag++;
	}
	if(flag==1) return true;
	else return false;
}


std::string truncatable(int num) {
    if(containsZero(num)==1) return "none";
    else{
	int left=0,right=0,rem,num1=num;
	int count1=noOfDigits(num);
	std::vector<int> leftNum;
	std::vector<int> rightNum;
	int tens=10,tempCount=0;
	while(tempCount<count1)
    {
        rem=num%tens;
        leftNum.push_back(rem);
        tens=tens*10;
        tempCount++;
        
    }
	tens=1;
	tempCount=0;
	while(tempCount<count1)
    {
        rem=num1/tens;
        rightNum.push_back(rem);
        tens=tens*10;
        tempCount++;
        
    }
	for(int index=0;index<leftNum.size();index++)
		if(isPrime(leftNum[index])) left++;
	for(int index=0;index<rightNum.size();index++)
		if(isPrime(rightNum[index])) right++;
	if(right==count1 && left==count1) return "both";
	else if(right==count1 && left!=count1) return "right";
	else if(right!=count1 && left==count1) return "left";
	else return "none";
    }	
	
}
